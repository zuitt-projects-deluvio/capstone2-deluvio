const jwt = require('jsonwebtoken');
const secret = "CourseBookingAPI";


module.exports.createAccessToken = (user) => {
	const data = {
		id: user.id,
		email: user.email,
		isAdmin: user.isAdmin
	}

	return jwt.sign(data, secret, {})
};


module.exports.verify = (req, res, next) => {

	let token = req.headers.authorization

	if (typeof token === "undefined"){
		return res.send({auth: "Failed. No token."})
	}	else {

		token = token.slice(7, token.length)
	}

	jwt.verify(token, secret, (err, decodedToken) => {

		if(err) {
			return res.send({
				auth: "Failed",
				message: err.message
			})
		} else {

			req.user  = decodedToken;
			
			next();
		}
	})
};


module.exports.verifyAdmin = (req, res, next) => {

	if(req.user.isAdmin){
		next();
	} else {
		return res.send({
			auth: "Failed",
			message : "Forbidden Action, you are not an Admin."
		})
	}

};


module.exports.decode = (token) => {

	if(typeof token !== "undefined"){

		token = token.slice(7, token.length);

		return jwt.verify(token, secret, (err, data) => {

			if(err){

				return null

			} else {

				return jwt.decode(token, {complete: true}).payload
			} 
		})

	} else {
		
		return null
	}
}
