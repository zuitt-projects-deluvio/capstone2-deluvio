const mongoose = require('mongoose');

let userSchema = new mongoose.Schema({

	firstName: {
		type: String,
		required: [true, "First Name is required"]
	},

	lastName: {
		type: String,
		required: [true, "Last Name is required"]
	},
	
	mobileNo: {
		type: String,
		required: [true, "Mobile Number is required"]
	},

	email: {
		type: String,
		required: [true, "Email is required"]
	},

	password: {
		type: String,
		required: [true, "Password is required"]
	},

	isAdmin: {
		type: Boolean,
		default: false
	},

	moviesPurchased: [
		{
			movieId: {
				type: String,
				required: [true, "Movie ID is required"]
			},

			movieName: {
				type: String,
				required: [true, "Movie Name is required"]
			},

			price: {
				type: Number,
				required: [true, "Movie Price is required"]
			},

			datePurchased: {
				type: Date,
				default: new Date()
			},

			status: {
				type: String,
				default: "Owned"
			}
		}
	],

	orders: [

		{
			orderId: {
				type: String,
				required: [true, "Password is required"]
			},

			datePurchased: {
				type: Date,
				default: new Date()
			}
		}
	]

});

module.exports = mongoose.model("User", userSchema);



