const mongoose = require('mongoose');

let movieSchema = new mongoose.Schema({

	movieName: {
		type: String,
		required: [true, "Movie Name is required"]
	},

	movieDescription: {
		type: String,
		required: [true, "Description is required"]
	},

	price: {
		type: Number,
		required: [true, "Price is required"]
	},

	isActive: {
		type: Boolean,
		default: true
	},

	createdOn: {
		type: Date,
		default: new Date()
	},

	movieImg: {
		type: String,
		required: [true, "Description is required"]
	},

	movieOwners: [
		{
			userId: {
				type: String,
				required: [true, "User ID is required"]
			},
			
			datePurchased: {
				type: Date,
				default: new Date()
			},

			status: {
				type: String,
				default: "Owned"
			}
		}
	]

});

module.exports = mongoose.model("Movie", movieSchema);