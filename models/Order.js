const mongoose = require('mongoose');

let orderSchema = new mongoose.Schema({

	totalAmount: {
		type: Number,
		required: [true, "Amount/Price is required"]
	},

	purchasedOn: {
		type: Date,
		default: new Date()
	},

	purchaser: [
		{
			userId: {
				type: String,
				required: [true, "User ID is required"]
			},
			
			datePurchased: {
				type: Date,
				default: new Date()
			},

			status: {
				type: String,
				default: "Owned"
			}
		}
	],

	productPurchased: [
		{
			movieId: {
				type: String,
				required: [true, "Movie ID is required"]
			},

			datePurchased: {
				type: Date,
				default: new Date()
			},

			status: {
				type: String,
				default: "Owned"
			}
		}
	]

});

module.exports = mongoose.model("Order", orderSchema);