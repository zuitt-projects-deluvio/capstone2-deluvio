const express = require('express');
const router = express.Router();

const userControllers = require("../controllers/userControllers");

const auth = require("../auth");

const {verify, verifyAdmin} =  auth;


// ROUTERS //

// Checking Email
router.post("/checkEmail", (req, res) => {
	userControllers.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController))
});

// User Registration
router.post("/register", userControllers.registerUser);

// User authentication
router.post("/login", userControllers.login);

// Set user as admin (admin only)
router.put("/updateAdmin/:id", verify, verifyAdmin, userControllers.updateAdmin)

// get all users
router.get("/", userControllers.getAllUsers)

// Non-admin User checkout (Create order)
router.post("/checkoutOrder/:movieId", verify, userControllers.checkoutOrder);

// retrieve order for single user
router.get("/getMyOrders", verify, userControllers.getMyOrders)

// get details of user for checking if logged in or not
router.post("/details", verify, userControllers.getProfile)



module.exports = router;

