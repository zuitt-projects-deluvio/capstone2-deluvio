const express = require('express');
const router = express.Router();

const orderControllers = require("../controllers/orderControllers");

const auth = require("../auth");

const {verify, verifyAdmin} =  auth;


// ROUTERS //

// get all orders
router.get("/allOrders", verify, verifyAdmin, orderControllers.allOrders)



module.exports = router;

