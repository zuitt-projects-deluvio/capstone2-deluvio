const express = require('express');
const router = express.Router();

const movieControllers = require("../controllers/movieControllers");

const auth = require("../auth");

const {verify, verifyAdmin} =  auth;


// ROUTERS //

// Retrieve all products
router.get("/",verify, verifyAdmin, movieControllers.getAllMovies);

// Retrieve All Active Products
router.get("/getActiveMovies", movieControllers.getActiveMovies);

// Retrieve a single product
router.get("/getSingleMovie/:id", movieControllers.getSingleMovie);

// Create Product (admin only)
router.post("/addMovie", verify, verifyAdmin, movieControllers.addMovie);

// Update product information (admin only)
router.put("/updateMovieInfo/:id", verify, verifyAdmin, movieControllers.updateMovieInfo);

// Archive Product (admin only)
router.put("/archiveMovie/:id", verify, verifyAdmin, movieControllers.archiveMovie);

// Activate Product (admin only)
router.put("/activateMovie/:id", verify, verifyAdmin, movieControllers.activateMovie);




module.exports = router;

