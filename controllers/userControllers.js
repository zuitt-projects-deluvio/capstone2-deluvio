const bcrypt = require('bcrypt')
const auth = require("../auth");

const User = require("../models/User");
const Movie = require("../models/Movie");
const Order = require("../models/Order");

// CONTROLLERS //


// Check if email exists

module.exports.checkEmailExists = (reqBody) => {
	
	return User.find({email: reqBody.email}).then(result => {
		if(result.length > 0){

			return true

		} else {
			 return false
		}
	})
}

// register user
module.exports.registerUser = (req, res) => {

	const hashedPW = bcrypt.hashSync(req.body.password, 10);

	let newUser = new User({
		firstName: req.body.firstName,
		lastName: req.body.lastName,
		mobileNo: req.body.mobileNo,
		email: req.body.email,
		password: hashedPW
	})

	// newUser.save()
	// 
	// .catch(err => res.send(err))

	return newUser.save()
		.then(user => res.send(true))
		.catch(err => res.send(false))
}


// module.exports.registerUser = (reqBody) => {

// 	let newUser = new User({
// 		firstName : reqBody.firstName,
// 		lastName : reqBody.lastName,
// 		email : reqBody.email,
// 		mobileNo : reqBody.mobileNo,
// 		password : bcrypt.hashSync(reqBody.password, 10)
// 	})
//     //salt => number of iterations that the data will undergo to mask the value.
// 	//MASK the real of the data. 

// 	return newUser.save().then((user, err) => {

// 		if(err){

// 			 return false

// 		} else {

// 			return true
// 		}
// 	})
// }


// get user details
module.exports.getProfile = (req, res) => {

	const userData = auth.decode(req.headers.authorization)
	

	User.findById(userData.id)
	.then(result => {

		result.password = " ";

		res.send(result)

	})

	
	
	

	// User.findById(userData.id)
	// .then(result => {
	// 	result.password = "";
	// 	res.send(result.password)
	// })

};


// get all users
module.exports.getAllUsers = (req, res) => {

User.find({})
.then(result => res.send(result))
.catch(err => res.send(err));

}


// Login

module.exports.login = (req, res) => {

	User.findOne({email: req.body.email})
	.then(foundUser => {
		if(foundUser === null){
			return res.send({message: "User does not exist."})
		} else {
			const isPasswordCorrect = bcrypt.compareSync(
				req.body.password, foundUser.password)

			if(isPasswordCorrect){
				return res.send({accessToken: auth.
					createAccessToken(foundUser)})
			} else {
				return res.send("Password is incorrect")
			}
		}
	})
	.catch(err => res.send(err));

};


// Update Admin
 module.exports.updateAdmin = (req, res) => {

 	let updates = {
 		isAdmin: true
 	}

 	User.findByIdAndUpdate(req.params.id, updates, {new: true})
 	.then(updatedUser => res.send(updatedUser))
 	.catch(err => res.send(err))

 }


// Non-admin User checkout (Create order)
module.exports.checkoutOrder = async (req, res) => {

	// checks if the movie is already bought
	let isMovieAlreadyBought = await User.findById(req.user.id)
		.then(user => {

			let checkMatch = user.moviesPurchased.map(movie => {
				return movie.movieId
			})

			return checkMatch.indexOf(req.params.movieId)

		})
		.catch(err => res.send(err))


	if(isMovieAlreadyBought !== -1){


		Movie.findById(req.params.movieId)
		.then(result => {

			result.movieOwners = "";
			return res.send([{message: "You have already purchased this movie!"}, result])
		})
		return
	} 


	// gets the price of the movie to be appended on the new order
	let moviePrice = await Movie.findById(req.params.movieId)
		.then(result => {
			
			return result.price;
		})
		.catch(err => {return err})

	// gets the name of the movie to be appended on the user
	let movieName = await Movie.findById(req.params.movieId)
		.then(result => {
			
			return result.movieName;
		})
		.catch(err => {return err})




	// creates a new order and saves it to database
	let newOrder = new Order({
		totalAmount: moviePrice
	});


	// get new order id
	let newOrderId = await newOrder.save()
		.then(neworder => {

			return neworder.id

		})
		.catch(err => res.send(err))


	// save purchaser to the new order created
	let isOrderPurchaserUpdated = await	Order.findById(newOrderId)
		.then(order => {

			let newPurchaser = {
				userId: req.user.id
			};

			order.purchaser.push(newPurchaser);

			return order.save().then(order => true).catch(err => err.message)
		})


	if(isOrderPurchaserUpdated !== true){
  		return res.send({message: isOrderPurchaserUpdated})
  	}


  	// save productPurchased to new Order created
	 let isOrderProductPurchasedUpdated = await Order.findById(newOrderId)
	 	.then(order => {

	 		let newProductPurchased = {
	 			movieId: req.params.movieId
	 		};
		
	 		order.productPurchased.push(newProductPurchased);
		
	 		return order.save().then(order => true).catch(err => err.message)
	 	}) 


	 if(isOrderProductPurchasedUpdated !== true){
	 	return res.send({message: isOrderProductPurchasedUpdated})
	 };


	//save new movieId to User
	let isUserUpdated = await User.findById(req.user.id)
		.then(user => {

			let moviesPurchased = {
				movieId: req.params.movieId,
				movieName: movieName,
				price: moviePrice
			}

			
			user.moviesPurchased.push(moviesPurchased);
			
			return user.save().then(user => true).catch(err => err.message)

		})

	if(isUserUpdated !== true){
		return res.send({message: isUserUpdated})
	};

	//save new orderId to User
	let isUserUpdated2 = await User.findById(req.user.id)
		.then(user => {
				

			let order = {
				orderId: newOrderId
			}

			
			user.orders.push(order);
			
			return user.save().then(user => true).catch(err => err.message)

		})

	if(isUserUpdated2 !== true){
		return res.send({message: isUserUpdated})
	};




	// save new orderid and userid to Movie			
	let isMovieUpdated = await Movie.findById(req.params.movieId)
		.then(movieRes => {

			let newMovieOwner = {
				userId: req.user.id,
			}
			
			movieRes.movieOwners.push(newMovieOwner);
			
			return movieRes.save().then(order => true).catch(err => err.message)

		})


	if (isMovieUpdated !== true){
		return res.send({message: isMovieUpdated})
	};


	if(isMovieUpdated && isUserUpdated && isOrderProductPurchasedUpdated && isOrderPurchaserUpdated){

		Movie.findById(req.params.movieId)
		.then(result => {

			result.movieOwners = "";
			return res.send([{message: 'Order created successfully'},result])
		})
		.catch(err => res.send(err))

		
	};





}


// retrieve order/s for single user
module.exports.getMyOrders = (req, res) => {

	User.findById(req.user.id)
	.then(result => res.send(result.orders))
	.catch(err => res.send(err))
}

