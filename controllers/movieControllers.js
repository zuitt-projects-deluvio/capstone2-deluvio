const bcrypt = require('bcrypt')
const Movie = require("../models/Movie");

const auth = require("../auth");


// CONTROLLERS


// Get all movies
module.exports.getAllMovies = (req, res) => {

	Movie.find({})
	.then(result => res.send(result))
	.catch(err => res.send(err))
}


// Add new movie
module.exports.addMovie = (req, res) => {

	let newMovie = new Movie({
		movieName: req.body.movieName,
		movieDescription: req.body.movieDescription,
		price: req.body.price,
		movieImg: req.body.movieImg
	})

	newMovie.save()
	.then(movie => res.send(movie))
	.catch(err => res.send(err))
}

// Retrieve All Active Products
module.exports.getActiveMovies = (req, res) => {

	Movie.find({isActive: true})
	.then(result => {

		if(result.length === 0){
			res.send({message: "There are no active movies"})
		} else {
			result.forEach(movie => {
				movie.movieOwners = ""
			})
			// console.log(result)
			res.send(result)
		}
		
	})
	.catch(err => res.send(err));

}

// Retrieve a single product
module.exports.getSingleMovie = (req, res) => {

	Movie.findById(req.params.id)
	.then(result => {

		result.movieOwners = "";
		res.send(result)
	})
	.catch(err => res.send(err))

}


// Update product information (admin only)
module.exports.updateMovieInfo = (req, res) => {

	let updates = {
		movieName: req.body.movieName,
		movieDescription: req.body.movieDescription,
		price: req.body.price
	};

	Movie.findByIdAndUpdate(req.params.id, updates, {new: true})
	.then(updatedMovie => res.send(updatedMovie))
	.catch(err => res.send(err));
}



// Archive a course
module.exports.archiveMovie = (req, res) => {

	Movie.findById(req.params.id)
	.then(movieStatus => {

		if(movieStatus.isActive === false){
			res.send("Movie is already archived")
		} else {

			let updates = {
				isActive: false
			}

			Movie.findByIdAndUpdate(req.params.id, updates, {new: true})
			.then(archivedMovie => res.send(archivedMovie))
			.catch(err => res.send(err))

		}

	})
	.catch(err => res.send(err));

};

// Activate movie
module.exports.activateMovie = (req, res) => {

	Movie.findById(req.params.id)
		.then(movieStatus => {

			if(movieStatus.isActive === true){
				res.send("Movie is already active")
			} else {

				let updates = {
					isActive: true
				}

				Movie.findByIdAndUpdate(req.params.id, updates, {new: true})
				.then(activatedMovie => res.send(activatedMovie))
				.catch(err => res.send(err))

			}

		})
		.catch(err => res.send(err));

};









// // Retrieve Active Courses
// module.exports.getActiveCourse = (req, res) => {

// 	Course.find({isActive: true})
// 	.then(result => res.send(result))
// 	.catch(err => res.send(err));

// };

// // Retrieve Active Courses
// module.exports.getInactiveCourse = (req, res) => {

// 	Course.find({isActive: false})
// 	.then(result => res.send(result))
// 	.catch(err => res.send(err));

// };


// // Update Course
// module.exports.updateCourse = (req, res) => {

// 	let updates = {
// 		name: req.body.name,
// 		description: req.body.description,
// 		price: req.body.price
// 	};

// 	Course.findByIdAndUpdate(req.params.id, updates, {new: true})
// 	.then(updatedCourse => res.send(updatedCourse))
// 	.catch(err => res.send(err));
// };


// // find courses by name

// module.exports.findCoursesByName = (req, res) => {

// 	Course.find({name: {$regex: req.body.name, $options: '$i'}})
// 	.then(result => {

// 		if(result.length === 0){
// 			return res.send("No courses found")
// 		} else {
// 			return res.send(result)
// 		}

// 	})
// 	.catch(err => res.send(err))
// }


// // find course by price

// module.exports.findCoursesByPrice = (req, res) => {

// 	Course.find({price: req.body.price})
// 	.then(result => {

// 		console.log(result);

// 		if(result.length === 0){
// 			return res.send("No courses found")
// 		} else {
// 			return res.send(result)
// 		}

// 	})
// 	.catch(err => res.send(err))

// }

// // get course enrollees

// module.exports.getEnrollees = (req, res) => {

// 	Course.findById(req.params.id)
// 	.then(result => res.send(result.enrollees))
// 	.catch(err => res.send(err))

// }