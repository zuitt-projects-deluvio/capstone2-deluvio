const bcrypt = require('bcrypt')
const auth = require("../auth");

const User = require("../models/User");
const Order = require("../models/Order");
const Movie = require("../models/Movie");


// CONTROLLERS //


// Retrieve all orders
module.exports.allOrders = (req, res) => {

	Order.find({})
	.then(result => res.send(result))
	.catch(err => res.send(err))

}

