const express = require('express');
const mongoose = require('mongoose');

const cors = require('cors');

const userRoutes = require('./routes/userRoutes');
const movieRoutes = require('./routes/movieRoutes')
const orderRoutes = require('./routes/orderRoutes')

const port = process.env.PORT || 4000;
const app = express();


mongoose.connect("mongodb+srv://admin_deluvio:admin169@deluvio-batch-169.rcgxg.mongodb.net/bookingAPI169forcapstone3?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true
});


let db = mongoose.connection;
db.on('error', console.error.bind(console, 'Connection Error'));
db.once('open', () => console.log('Connected to MongoDB'));


//middlewares
app.use(express.json());
app.use(cors());


app.use('/users', userRoutes);
app.use('/movies', movieRoutes);
app.use('/orders', orderRoutes);


app.listen(port, () => console.log(`Server is running at port ${port}`));

// https://fathomless-forest-51958.herokuapp.com/